//
//  ViewController.swift
//  June
//
//  Created by Chaman Gujjar on 02/06/15.
//  Copyright (c) 2015 Chaman Gujjar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
var isInMiddleOfNumber = false
    var firstOperand:Double?
    var operation:String?
    
    @IBOutlet weak var resultTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func digitButtonPressed(sender: UIButton) {
        
        if isInMiddleOfNumber{
        resultTextField.text =
            resultTextField.text + sender.currentTitle!
        }
        else
        {
            resultTextField.text = sender.currentTitle
            
        }
        
        isInMiddleOfNumber = true
        
        
        println("Button \(sender.currentTitle) pressed")
        
        
    }
    
    
    @IBAction func operatiionButtonPressed(sender: UIButton) {
        
        isInMiddleOfNumber = false

        calculate()
        
        firstOperand = NSNumberFormatter().numberFromString(resultTextField.text)!.doubleValue
        
   operation = sender.currentTitle!
        
        println("Button \(sender.currentTitle) pressed")
        
        
    }
    
    
    
    @IBAction func calculate() {
        
      isInMiddleOfNumber = false
        
        if operation != nil {
            
            var result:Double
            var  secondOperand = NSNumberFormatter().numberFromString(resultTextField.text)!.doubleValue
            
        
            switch operation! {
                
                case "+":
                result = firstOperand! + secondOperand
                
                case "−":
                result = firstOperand! - secondOperand
                
                case "×":
                result = firstOperand! * secondOperand
                
                case "÷":
                
                result = firstOperand! / secondOperand
            default:
                result = 0.0
                
            }
            resultTextField.text = "\(result)"
            
        }
        }
    
    
    @IBAction func clear() {
        resultTextField.text = "0"
        operation = nil
        firstOperand = nil
        isInMiddleOfNumber = false
        
    }
    
    
    @IBAction func backspace() {
        
        if count(resultTextField.text)>1 {
            var index = resultTextField.text.endIndex.predecessor()
            
            
            resultTextField.text = resultTextField.text.substringToIndex(index)
            
        } else{
            
            resultTextField.text = "0"
        }
        }
    
    }
    
    




